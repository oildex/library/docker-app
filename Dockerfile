FROM docker:19.03.0-rc2

ARG DOCKER_APP_VERSION=v0.8.0
RUN wget https://github.com/docker/app/releases/download/$DOCKER_APP_VERSION/docker-app-linux.tar.gz && \
    tar -xzf docker-app-linux.tar.gz --exclude docker-app-standalone-linux && \
    chown root:root docker-app-plugin-linux && \
    mkdir -p /usr/lib/docker/cli-plugins && \
    mv docker-app-plugin-linux /usr/lib/docker/cli-plugins/docker-app && \
    rm docker-app-linux.tar.gz

CMD ["/usr/local/bin/docker", "app"]
