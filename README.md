# Docker Application

Docker Application (docker-app) is a Docker CLI plugin for application packages. See the [docker/app](https://github.com/docker/app) project for details.

This project extends the [docker](https://hub.docker.com/_/docker/) image with the docker-app CLI plugin. There is an open issue at [docker-library/docker#156](https://github.com/docker-library/docker/issues/156) to request that the docker-app CLI plugin be included with the docker image.
